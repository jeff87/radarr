FROM hotio/radarr
LABEL maintainer="jeffwieland"

RUN apt-get update --allow-insecure-repositories && \
    apt-get install -y software-properties-common && \
    add-apt-repository ppa:jonathonf/ffmpeg-4 && \
    apt-get update --allow-insecure-repositories && \
    apt-get install -y python-pip python-setuptools ffmpeg && \
    pip --no-cache-dir install requests requests[security] requests-cache babelfish "guessit<2" "subliminal<2" && \
    pip uninstall -y stevedore && \
    pip --no-cache-dir install stevedore==1.19.1 python-dateutil deluge-client qtfaststart && \
    rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*
